var express = require('../config/express')();
var request = require('supertest')(express);

describe('#ProdutosController', function () {

    beforeEach(function (done) {
        var conn = express.infra.dbConnection();
        conn.query("delete from produtos", function (err, result) {
            if(!err)
            {
                done();
            }
        });
    });

    it('#listagem json', function (done) {

        request.get('/produtos')
            .set('Accept', 'text/html')
            .expect('Content-Type', /html/)
            .expect(200, done);
    });

    it('#Cadastro de novo produto com dados invalidos', function (done) {
        request.post('/produtos')
            .send({ titulo: "", descricao: "novo livro" })
            .expect(400, done);
    });

    it('#Cadastro de novo produto com dados validos', function (done) {
        request.post('/produtos')
            .send({ titulo: "Novo livro testes", descricao: "novo livro", preco: 20.50 })
            .expect(302, done);
    });
});