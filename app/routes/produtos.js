
module.exports = function (app) {
    app.get('/produtos', function (req, res, next) {
        var connection = app.infra.dbConnection();
        var produtosDAO = new app.infra.ProdutosDAO(connection);

        produtosDAO.listar(function (err, results) {

            if (err)
                return next(err);

            res.format({
                html: function () {
                    res.render('produtos/lista', { lista: results });
                },
                json: function () {
                    res.json(results);
                },
                text: function () {
                    res.send(results);
                }
            });
        });
        connection.end();
    });

    app.get('/produtos/form', function (req, res) {
        res.render('produtos/form', { errosValidacao: {}, produto: {} });
    });

    app.post('/produtos', function (req, res) {
        var connection = app.infra.dbConnection();
        var produtosDAO = new app.infra.ProdutosDAO(connection);

        req.assert('titulo', 'titulo é obrigatório').notEmpty();
        req.assert('preco', 'Formato inválido').isFloat();

        var erros = req.validationErrors();
        if (erros) {
            res.format({
                html: function () {
                    res.status(400).render('produtos/form', { errosValidacao: erros, produto: req.body });
                },
                json: function () {
                    res.status(400).json(erros);
                },
                text: function () {
                    res.status(400).send(erros);
                }
            });
            return;
        }

        produtosDAO.salvar(req.body, function (err, results) {
            res.redirect('/produtos');
        });
    });
}
